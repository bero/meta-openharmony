# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

SRC_URI += "file://patches/base_user_iam_pin_auth.patch;apply=no;subdir=src"
